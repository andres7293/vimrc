call plug#begin()
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'godlygeek/tabular'
Plug 'Valloric/youcompleteme'
Plug 'vim-airline/vim-airline'
call plug#end()

"" Fix backspace indent
set backspace=indent,eol,start

"" Tabs. May be overriten by autocmd rules
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab
set nu
set hls
set autoindent

"" Map leader to ,
let mapleader=','

nnoremap <silent> <F2> :NERDTreeFind<CR>
nnoremap <silent> <F3> :NERDTreeToggle<CR>

"" Git
noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gsh :Gpush<CR>
noremap <Leader>gll :Gpull<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gb :Gblame<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>gr :Gremove<CR>

" git-gutter
set updatetime=100

"" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>q :bp<CR>
noremap <leader>x :bn<CR>
noremap <leader>w :bn<CR>

" C files configuration
autocmd Filetype c inoremap {   {}<Left>
autocmd Filetype c inoremap [   []<Left>
autocmd Filetype c inoremap (   ()<Left>
autocmd Filetype c inoremap '   ''<Left>
autocmd Filetype c inoremap "   ""<Left>
autocmd Filetype c inoremap <   <><Left>
autocmd Filetype c inoremap #include #include <Left><>
autocmd Filetype c inoremap struct struct {};<Left><Left><Left>
autocmd Filetype c inoremap enum enum {};<Left><Left><Left>
autocmd Filetype c inoremap printf printf("");<Left><Left><Left>
autocmd Filetype c noremap <Leader>m<CR> :!make

" cpp files configuration
autocmd Filetype cpp inoremap {   {}<Left>
autocmd Filetype cpp inoremap [   []<Left>
autocmd Filetype cpp inoremap (   ()<Left>
autocmd Filetype cpp inoremap '   ''<Left>
autocmd Filetype cpp inoremap "   ""<Left>
autocmd Filetype cpp inoremap #in #include <Left>
autocmd Filetype cpp inoremap class class {};<Left><Left><Left>
autocmd Filetype cpp inoremap struct struct {};<Left><Left><Left>
autocmd Filetype cpp inoremap enum enum {};<Left><Left><Left>
autocmd Filetype cpp inoremap printf printf("");<Left><Left><Left>
autocmd Filetype cpp inoremap cout cout <<
autocmd Filetype cpp inoremap this this->
autocmd Filetype cpp noremap <Leader>m :!make<CR>

" python files configuration
autocmd Filetype python inoremap {   {}<Left>
autocmd Filetype python inoremap [   []<Left>
autocmd Filetype python inoremap (   ()<Left>
autocmd Filetype python inoremap '   ''<Left>
autocmd Filetype python inoremap "   ""<Left>
autocmd Filetype python inoremap print  print("")<Left><Left>

" javascript files configuration
autocmd Filetype javascript inoremap {   {}<Left>
autocmd Filetype javascript inoremap [   []<Left>
autocmd Filetype javascript inoremap (   ()<Left>
autocmd Filetype javascript inoremap '   ''<Left>
autocmd Filetype javascript inoremap "   ""<Left>
autocmd Filetype javascript inoremap <   <><Left>
autocmd Filetype javascript inoremap console.log console.log("")<Left><Left>
